resource "google_pubsub_topic" "this" {
  name = var.topic_name
  kms_key_name = var.kms_key_name
  labels = var.labels
  project =  var.project_id
    
  message_storage_policy {
    allowed_persistence_regions = var.allowed_regions
  }
}