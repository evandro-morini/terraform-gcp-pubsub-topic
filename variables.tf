variable "topic_name" {
    type = string
    description = "Topic Name"
}

variable "kms_key_name" {
    type = string
    description = "Cloud KMS CryptoKey for topic content encryption"
    default = ""
}

variable "labels" {
    type = map
    description = "Labels on key:value format"
    default = {}
}

variable "project_id" {
    type = string
    description = "Project's ID on GCP"
}

variable "allowed_regions" {
  type = list(string)
  description = "Default regions allowed"
  default = [
      "southamerica-east1"
  ]
}