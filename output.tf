output "google_pubsub_topic_id" {
    value = "${google_pubsub_topic.this.id}"
}

output "google_pubsub_topic_name" {
    value = "${google_pubsub_topic.this.name}"
}